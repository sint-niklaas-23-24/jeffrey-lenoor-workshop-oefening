﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Workshop_oefening
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        BitmapImage bitmap = new BitmapImage(new Uri("C:\\Users\\Student\\Desktop\\Nieuwe map\\Workshop oefening\\Workshop oefening\\user icon.png"));

        List<Persoon> listPersoon = new List<Persoon>();

        private void btnOpslaan_Click(object sender, RoutedEventArgs e)
        {
            Persoon persoon1 = new Persoon(txtVoornaam.Text, txtNaam.Text);
            persoon1.Avatar = bitmap;

            listPersoon.Add(persoon1);

            cmbCursisten.ItemsSource = null; //Deze cleared de binding om zo opnieuw iets nieuws toe te voegen in de combobox
            cmbCursisten.ItemsSource = listPersoon;

            MessageBox.Show("Welkom " + persoon1.Details() + "!");

            txtNaam.Clear();
            txtVoornaam.Clear();

        }
    }
}
