﻿using System;

namespace Workshop_oefening
{
    internal class Cursist : Persoon
    {
        //Attributen
        private Guid _cursistennummer;

        //Constructors
        public Cursist()
        {

        }

        public Cursist(Guid eenGuid)
        {
            Cursistennummer = eenGuid;
        }

        //Properties
        public Guid Cursistennummer
        {
            get { return _cursistennummer; }
            set { _cursistennummer = value; }
        }

        //Methoden
        public override string Details()
        {
            return base.Details() + " " + Cursistennummer;
        }
    }
}
