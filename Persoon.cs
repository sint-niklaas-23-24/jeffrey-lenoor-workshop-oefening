﻿using System.Windows.Media;

namespace Workshop_oefening
{
    internal class Persoon
    {
        //Attributen
        private string _achternaam;
        private string _voornaam;
        private ImageSource _avatar;

        //Constructors
        public Persoon()
        {

        }

        public Persoon(string eenVoornaam, string eenAchternaam)
        {
            Achternaam = eenAchternaam;
            Voornaam = eenVoornaam;
        }
        //Properties
        public ImageSource Avatar
        {
            get { return _avatar; }
            set { _avatar = value; }
        }

        public string Achternaam
        {
            get { return _achternaam; }
            set { _achternaam = value; }
        }

        public string Voornaam
        {
            get { return _voornaam; }
            set { _voornaam = value; }
        }

        //Methoden
        public virtual string Details()
        {
            return Voornaam + " " + Achternaam;
        }
    }
}
