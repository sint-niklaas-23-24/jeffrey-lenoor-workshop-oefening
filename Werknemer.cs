﻿namespace Workshop_oefening
{
    internal class Werknemer : Persoon
    {
        //Attributen
        private int _loon;

        //Constructors
        public Werknemer()
        {

        }

        //Properties
        public int Loon
        {
            get { return _loon; }
            set { _loon = value; }
        }

        //Methoden
        public override string Details()
        {
            return base.Details();
        }

    }
}
